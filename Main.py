import sqlite3
import qrcode
import openpyxl
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from tkinter import Tk  # from tkinter import Tk for Python 3.x
from tkinter.filedialog import askopenfilename



def database_Main():

    # enable SQL database
    database = sqlite3.connect("ZSO_reagent.db")
    
    # create cursor
    cursor = database.cursor()

    # Create table
    # TODO Make separate tables for each real life room
    cursor.execute('''CREATE TABLE IF NOT EXISTS reagents
                (ID INTEGER PRIMARY KEY AUTOINCREMENT, 
                Name TEXT, 
                CAS UNIQUE, 
                Quantity INTEGER NOT NULL DEFAULT 0, 
                Amount TEXT, 
                Location TEXT)''')

    # Placeholder for QRCode
    # read data from database exported as txt file
    # TODO read directly from excel file or SQL database

    #lines_from_database = read_reagent_data()
    lines_from_database = read_excel_file()

    # Split data in each line from input file
    print("Data fromm excel file:")
    for database_line in lines_from_database:
        
        # I selected @ as divider, because it's a symbol never used in chemistry
        scanCode = database_line.split("@")
        
        print(scanCode)

        # Prepare data for insertion into the database
        cursor.execute("INSERT OR IGNORE INTO reagents(Name, CAS, Amount, Location) "
                    "VALUES(?,?,?,?)",
                    (scanCode[0], # Name
                    scanCode[1],  # CAS number
                    scanCode[2],  # Amount (mass or volume)
                    scanCode[3])) # Reagent location IRL

        # Push changes to database and update qunatities of chemicals that already exist in database
        database.execute("UPDATE reagents SET Quantity = Quantity + ? WHERE CAS = ?", ("1", scanCode[1]))

    # Exporting database data as QR code
    print("Data from database:")
    for row_data in cursor.execute('SELECT * FROM reagents ORDER BY ID'):
        qrCodeGenerator(row_data)
        
        print(row_data)


    # Save (commit) the changes
    database.commit()

    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    database.close()


def qrCodeGenerator(reagent_data):
    reagent_ID = str(reagent_data[0])
    reagent_name = str(reagent_data[1])
    reagent_CAS = str(reagent_data[2])
    reagent_amount = str(reagent_data[3])
    reagent_seller = "defualt seller"

    divider = "@"

    reagent_data = reagent_ID +divider+ reagent_name +divider+ reagent_CAS +divider+ reagent_amount +divider+ reagent_seller
    size = 2

    qr = qrcode.QRCode(
        version=None,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=size,
        border=10*(4/size),
    )

    qr.add_data(reagent_data)
    qr.make(fit=True)
    qr_ready = qr.make_image(fill_color="black", back_color="white")

    font = ImageFont.truetype("./arial.ttf", 10)

    reagent_name = convert_to_multiline(reagent_name, 30)
    ImageDraw.Draw(qr_ready).multiline_text((2*size, 0), reagent_name, font=font)

    qr_ready.save(reagent_ID + "_" + reagent_CAS + ".jpg")
    #qr_ready.show()




# read data from text file
def read_reagent_data():

    Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
    filename = askopenfilename()  # show an "Open" dialog box and return the path to the selected file

    lines = []
    clean_lines = []

    # open textfile and read lines from it
    with open(filename) as textfile:
        lines = textfile.readlines()
    
    # clean lines/remove unnecessary symbols like/n
    for line in lines:
        line = line.strip()

        # if line is not empty after that then add it to the output
        if line != "":
            clean_lines.append(line)

    
    #print(clean_lines)
    return clean_lines


def read_excel_file():

    Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing

    filename = askopenfilename()  # show an "Open" dialog box and return the path to the selected file

    wb = openpyxl.load_workbook(filename)
    ws = wb['2.106']

    excel_row_data = []

    for i in range(2, 20):
        extracted_row = ""
        for column in "ABCDFG":
            extracted_row = extracted_row + str(ws[column+ str(i)].value) + "@"

        extracted_row = extracted_row[:-1] # remove last letter which should be "@"
        excel_row_data.append(extracted_row)
    
    return excel_row_data
    #print(extracted_row)



# Support methods
def convert_to_multiline(string, line_length):

    new_string = ""

    if len(string) > line_length:
        new_string = string[:line_length] +"\n" + convert_to_multiline(string[line_length:], line_length)
    else:
        new_string = string

    return new_string





if __name__ == "__main__":

    database_Main()
    # qrCodeGenerator("")
    #read_reagent_data()

    #read_excel_file()

    print("Done")